import requests
import json
import re
import os
from datetime import datetime

# API Authentification
GHOST_URL = os.getenv('GHOST_URL')
endpoint_auth = f"{GHOST_URL}/ghost/api/admin/session"

payload_auth = json.dumps({
    "username": os.getenv('GHOST_USER'),
    "password": os.getenv('GHOST_PASSWORD')
})
headers_auth = {
    'Content-Type': 'application/json'
}

response_auth = requests.post(endpoint_auth, headers=headers_auth, data=payload_auth)

if response_auth.status_code == 201:
    # Store the cookie
    cookie_created = response_auth.headers.get("Set-Cookie")
    # RegEx to isolate the cookie
    isolate_cookie = re.findall(r"\b\S+", cookie_created)
    cookie_with_semicolon = isolate_cookie[0]
    cookie = cookie_with_semicolon.rstrip(cookie_with_semicolon[-1])

    headers_export = {
        'Cookie': cookie
    }
else:
    print("L'authentification a échoué, le code retour HTTP est : " + str(response_auth.status_code)) 
    exit(1)

# API Export
endpoint_export = f"{GHOST_URL}/ghost/api/admin/db/"

export_path = os.getenv('GHOST_EXPORT_DIRECTORY')

get_export = requests.get(url=endpoint_export, headers=headers_export)
get_export = get_export.json()
get_export = json.dumps(get_export, indent=2)

# Récupération de la date du jour
today = datetime.now().strftime("%d-%m-%Y")

export_file_name = f"export-{today}.json"

with open(os.path.join(export_path, export_file_name), 'w') as export:
    export.write(get_export)

# Récupération de la liste des fichiers dans le répertoire courant
files = os.listdir()

# Parcours de la liste des fichiers
for file in files:
    # Vérification si le fichier correspond au format du nom du fichier avec la date
    if file.startswith("export-") and file.endswith(".json"):
        # Récupération de la date du fichier
        date_str = file[7:-5]
        date = datetime.strptime(date_str, "%d-%m-%Y")

        # Calcul de la différence entre la date du jour et la date du fichier
        diff = datetime.now() - date

        # Si la différence est supérieure à 14 jours, suppression du fichier
        if diff.days > 14:
            os.remove(file)
