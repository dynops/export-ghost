# Presentation
This script do an export of your **GHOST** Blog Data into a `Json` file.
This script uses the Ghost's hidden API.

It keep 14 days of retention. Past this delay, the file will be removed.

# Prerequisites
You have to export these variables :
```bash
export GHOST_USER='YOUR GHOST USER MAIL'
export GHOST_PASSWORD='YOUR PASSWORD GHOST USER'
export GHOST_EXPORT_DIRECTORY='PATH OF EXPORT FILE'
export GHOST_URL='FULL GHOST URL (example : https://dynops.fr)'
```
This user have to be administrator on **GHOST**.

# Usage
```python
python3 export-data.py
```
And a file `export.json` will be created at the PATH of the variable 'GHOST_EXPORT_DIRECTORY'

# Results
The file contains all your Data and can be imported on the web Interface of Ghost.   
![Export-Ghost](img/export-ghost.png)
